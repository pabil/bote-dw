export class Persona {
  id: string;
  nombre: string;
  email: string;
  aniadirGastos: boolean;
  aniadirPagos: boolean;
}
